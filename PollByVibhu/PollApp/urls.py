from django.urls import path

from . import views
from .views import vote, index, detail, results,fetch_based_on_pkey

urlpatterns = [
    path('', index, name='index'),  # /pollApp/
    path('<int:question_id>/', detail, name='detail'),   # /PollApp/5
    path('<int:question_id>/results/', results, name='results'),   # /PollApp/5/results/
    path('<int:question_id>/vote/', vote, name='vote'),  # /PollApp/5/vote/
    path('demo', views.demo, name='first_api'),
    path('fetch_question', fetch_based_on_pkey, name='fetch_question')

]

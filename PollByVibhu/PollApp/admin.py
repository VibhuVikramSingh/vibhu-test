from django.contrib import admin
from .models import Question, Choice


admin.site.register(Question)
admin.site.register(Choice)


# @admin.register(Question)
# class QuestionAdmin(admin.ModelAdmin):
#     list_display = ['id', 'pub_date', 'question_text']






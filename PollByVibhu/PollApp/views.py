from django.http import HttpResponse
from rest_framework import status, serializers
from rest_framework.response import Response

from .models import Question
from django.template import loader
from rest_framework.decorators import api_view, authentication_classes, permission_classes


def index(request):
    print("inside views.index")
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    template = loader.get_template('PollApp/index.html')
    context = {
        'latest_question_list': latest_question_list,
    }
    output = ','.join([q.question_text for q in latest_question_list])
    return HttpResponse(template.render(context, request))


def detail(request, question_id):
    print("inside detail method")
    return HttpResponse("You're looking at question %s." % question_id)


def results(request, question_id):
    print("inside results method")
    response = "you're looking at the results of question %s."
    return HttpResponse(response % question_id)


def vote(request, question_id):
    print("inside vote method")
    return HttpResponse("You're voting on question %s." % question_id)


# @api_view(['GET'])
# def demo(request):
#     return Response("You're voting on question %s.")

@api_view(['POST'])
def demo(request):
    num1 = request.data.get('number1')
    num2 = request.data.get('number2')
    sum = num1 + num2
    status_dict = {'status': 'Hello working'}
    return Response(sum, status=status.HTTP_201_CREATED)


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        # fields =['id'] --> if want to serialize a specific field else as below
        fields = '__all__'


@api_view(['GET'])
@permission_classes([])
@authentication_classes([])
def fetch_based_on_pkey(request):
    print('in api.....')
    id_to_fetch = request.GET.get('id_from_front_end')
    if not id_to_fetch:
        return Response({'msg': 'Please enter the ID'}, status=status.HTTP_400_BAD_REQUEST)
    fetched_question = None
    try:
        fetched_question = Question.objects.filter(id=id_to_fetch) #returns complex data which is a query set object
        #use .filter and not .get as .get throws exception if results are anything except 1 data set. .filter prevents this
        print(fetched_question)
    except Exception as e:
        return Response({'Error': e}, status=status.HTTP_400_BAD_REQUEST)
    # question_dict = {"Question_Text": fetched_question.question_text, "Publication_date": fetched_question.pub_date}
    question_dict = QuestionSerializer(fetched_question, many=True).data  #.data converts to dictionary
    print(question_dict, '---------question')
    return Response(question_dict, status=status.HTTP_200_OK)


